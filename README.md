# rb-container

É um container para gestão de denpencias que funciona com base em arquivos json ("dependencias.json") para inserção de dependencias, exemplo:

criando o arquivo /src/dependencias.json:

    {
      "Query": "\\Rubeus\\Query\\Query"
    }
  
criando o arquivo /src/app.php

    <?php
    require_once __DIR__.'/../vendor/autoload.php';
    use Rubeus\ContenerDependencia\Conteiner
      
    //retorna uma instancia da classe query('\Rubeus\Query\Query')
      
    $query = Conteiner::get('Query');


Armazenando e recuperando valores:
-----------------------------

    <?php
    require_once __DIR__.'/../vendor/autoload.php';
    use Rubeus\ContenerDependencia\Conteiner
      
    Conteiner::registrar('dominio', 'www.dominio.com.br');
      
    echo Conteiner::get('dominio');
    //output www.dominio.com.br
