<?php
namespace Rubeus\ContenerDependencia;
use Rubeus\Servicos\Json\Json;
use Rubeus\Servicos\Classe\Classe;

abstract class Conteiner{
    private static $pool;
    private static $class;
    private static $conf;
    
    private  static function iniciar(){
        self::$class = new Classe();
        self::$conf = Json::lerArq(DEPENDENCIA);
    }
    
    public static function limpar(){
        self::$pool = [];    
    }
    
    public static function registrar($interface,$valor){
        self::$pool[$interface] = $valor;    
    }
    
    public static function get($interface,$registrar=true){
        return self::getInstancia($interface,$registrar,1);    
    }
    
    public static function getDump(){
        var_dump(self::$pool);   
    }
    
    public static function getInstancia($interface,$registrar=true,$indice=0){
        if(is_null(self::$class)){
            self::iniciar();
        } 
        $backtrace = debug_backtrace();
        
        $arquivo = $backtrace[$indice]['file'];
        
        if(!isset(self::$pool[$interface])||!$registrar){
            if(strpos($arquivo,'vendor')){
                $instancia = self::criarInstancia($interface,DIR_BASE.self::$conf->dependencia_root);
            }else{
                $instancia = self::criarInstancia($interface,\Ambiente::dir($arquivo));
            }
        }else{
            $instancia = self::$pool[$interface];
        }
        if(!$instancia){
             if(strpos($arquivo,'vendor')){
                $instancia = self::criarInstancia($interface,\Ambiente::dir($arquivo));
            }else{
                $instancia = self::criarInstancia($interface,DIR_BASE.self::$conf->dependencia_root);
            }
        }
        
        if($registrar){
            self::$pool[$interface] = $instancia;
        }
        return $instancia;
    }
    
    private static function diretorio($arquivo){
        return substr($arquivo, 0, strripos( $arquivo, '/')); 
    }
    
    private static function criarInstancia($interface,$classe, $i=0){
        $diretorio = self::diretorio($classe);
       
        if(trim($diretorio) === ''){
            return false;
        }
       
        if(!file_exists($diretorio.'/dependencias.json')){
            return self::criarInstancia($interface, $diretorio, $i);
        }
       
        $objetoJson = Json::lerArq($diretorio.'/dependencias.json');
        
        if(isset($objetoJson->$interface)){
            $nomeClasse = $objetoJson->$interface;
            self::$class->substituir($nomeClasse);
            if(!self::$class->isAbstract()){
                return new $nomeClasse();
            }else{
                return $nomeClasse;
            }
        }
        return self::criarInstancia($interface, $diretorio, $i);
    }
}
